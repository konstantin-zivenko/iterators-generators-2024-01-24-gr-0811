from datetime import date, timedelta
from typing import Optional
from collections.abc import Iterator


class DateRange(Iterator):
    def __init__(
            self,
            start_stop_date: date,
            stop_date: Optional[date] = None,
            step: int = 1
    ) -> None:
        if not isinstance(start_stop_date, date):
            raise TypeError("start_stop_date must be datetime.date type")
        if not stop_date:
            self.stopdate = start_stop_date
            self.startdate = date.today()
        else:
            if not isinstance(stop_date, date):
                raise TypeError("stop_date must be datetime.date type")
            self.stopdate = stop_date
            self.startdate = start_stop_date
            if not self.startdate <= self.stopdate:
                raise ValueError("start date must be less or equal stop date")
        self.step = timedelta(days=step)
        self._current_itr_date = self.startdate - self.step

    def __next__(self) -> date:
        self._current_itr_date += self.step
        if self._current_itr_date >= self.stopdate:
            raise StopIteration

        return self._current_itr_date


if __name__ == "__main__":
    _data_1 = date(
        year=2023,
        month=10,
        day=25
    )

    _data_2 = date(
        year=2023,
        month=11,
        day=10
    )

    for day in DateRange(_data_1, _data_2, 3):
        print(day)