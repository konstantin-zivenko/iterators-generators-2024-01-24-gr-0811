from collections.abc import Iterator
from typing import Optional
from numbers import Number


class FloatRange(Iterator):
    def __init__(
            self,
            start_stop: float,
            stop: Optional[float] = None,
            step: float =  1.0
    ):
        if isinstance(stop, Number):
            self._start = start_stop
            self._stop = stop
        else:
            self._start = 0
            self._stop = start_stop

        self._step = step
        self._current = self._start

    def __next__(self) -> float:
        if self._current < self._stop:
            value = self._current
            self._current += self._step
            return float(value)
        raise StopIteration


if __name__ == "__main__":
    for i in FloatRange(5.5, 10, 0.3):
        print(i)
